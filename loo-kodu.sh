#!/bin/bash
#Autor: Karl-Erik Krais
#Skriptimiskeeled 2. kodutöö
#Kirjeldus: Loodetavasti teeb seda, mida vaja
#Versioon 1.1

export LC_ALL=C
export LANG=C


#Kontrollime, kas skript on käivitatud juurkasutajana:

if [ $UID -ne 0 ]
    then
        echo "Käivita skript juurkasutaja õigustes!"
    exit 1
fi

#Parameetrite kontroll:

if [ $# -eq 1 ]; then
VKODU=$1

    else
        echo "Kasutamine $0 www.minuveebisait.ee"
    exit 1
fi

#Kontrollime, kas Apache on paigaldatud, kui mitte, siis paigaldame:

type apache2 > /dev/null 2>&1

if [ $? -ne 0 ]
    then
 
    echo "Apache't hakatakse paigaldama..."
        apt-get update > /dev/null 2>&1 && apt-get install apache2 -y > /dev/null 2>&1 || exit1
        
    echo "Apache on paigaldatud!"

fi

#Loome nimelahenduse /etc/hosts failis :

echo "127.0.0.1 $VKODU" >> /etc/hosts

#Loome kausta /var/www kataloogi $VKODU:

mkdir -p /var/www/$VKODU

#Loome logide jaoks kausta

mkdir -p /var/www/logs


#Kopeerime index.html faili:

cp /var/www/html/index.html /var/www/$VKODU

#Kopeerime konfiguratsioonifaili:

cat /etc/apache2/sites-available/000-default.conf > /etc/apache2/sites-available/$VKODU.conf


#Muudame konfiguratsioonifaili:

#cat >> /etc/apache2/sites-available/$VKODU.conf << LOPP


sed -i "s-#ServerName www.example.com-ServerName $VKODU-g" /etc/apache2/sites-available/$VKODU.conf 
sed -i "s-/var/www/html-/var/www/$VKODU-g" /etc/apache2/sites-available/$VKODU.conf

#<VirtualHost *:80>                             #VANA KOOD
#        ServerName $VKODU
#        DocumentRoot /var/www/$VKODU
#        CustomLog /var/www/logs/$VKODU.log combined
#        ErrorLog /var/www/logs/$VKODU.err.log
#</VirtualHost>
#LOPP

#Muudame index.html faili:

cat > /var/www/$VKODU/index.html << LOPP

<html>
    <body>
            <h1>$VKODU </h1>
                <p> See on testleht </p>
    </body>
</html>

LOPP

#Lubame virtuaalserverite kasutamise:

a2ensite $VKODU > /dev/null 2>&1

#Teeme apache'le reloadi:

/etc/init.d/apache2 reload > /dev/null 2>&1


echo "Veebileht on loodud!"










